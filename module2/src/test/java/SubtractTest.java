import org.junit.Assert;
import org.junit.Test;

public class SubtractTest {

    @Test
    public void testSubtract() {
        Subtract subtract = new Subtract(5);
        Assert.assertEquals(1, subtract.subtract(4));
    }

//    @Test
//    public void testFail() {
//        Assert.assertTrue(false);
//    }
}
